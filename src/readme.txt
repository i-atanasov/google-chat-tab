# Introduction

Opens Google Chat in a Thunderbird tab. Once installed, you should see a button
for Google Chat. Clicking on it will open Google Chat within Thunderbird in a
tab.

## Usage

- Login to Google account as you would in the browser. 
- Frequent re-login may be required because Google logs out your session.
- You can log-out the same way you do in the browser
- OAuth credentials are stored in Thunderbird secrets manager
  Preferences -> Privacy & Security --> Saved Logins --> Search for `oauth://`
- Click on Show Passwords button to reveal OAuth secrets
- I recommend you use a Primary Password to protect saved passwords (save it in
  your password manager!)

# Version compatibility

- Tested with Thunderbird v91+. But should work on any version from v78.

# TBD

- Submit to AMO / ATN

## Contributing

See [Contributing](CONTRIBUTING.md) for more details on how you can help.

## License GPL-3


