function openGoogleChat() {
  browser.tabs.create({
    url: "https://mail.google.com/chat/u/0/#chat/welcome"
  });
}

browser.browserAction.onClicked.addListener(openGoogleChat);

