# How to contribute

- Fork the repo into your account, make changes and create a Merge Request

# Test your changes

- Go to Add-ons page: Menu --> `Addons and Themes`
- Click on the Gear icon and select `Debug Add-ons`
- In the debug page, click on `Load Temporary Add-on..` button and select your
  manifest.json file
- Test and remove your temporary Add-on later if you choose.

## References

* https://developer.thunderbird.net/add-ons/about-add-ons

